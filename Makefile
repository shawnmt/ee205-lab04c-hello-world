###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 04c - Hello C++
#
# @file    Makefile
# @version 1.0
#
# @author Shawn Tamashiro <shawnmt@hawaii.edu>
# @brief  Lab 04c - Hello C++ - EE 205 - Spr 2021
# @date   15 Feb 2021
###############################################################################

#this allows us to compile with just make
TARGETS = hello1 hello2

all: $(TARGETS)

#program 2
hello1: hello1.cpp
	g++ -o hello1 hello1.cpp

#program 2
hello2: hello2.cpp
	g++ -o hello2 hello2.cpp

clean:
	rm -f $(TARGETS) *.o


