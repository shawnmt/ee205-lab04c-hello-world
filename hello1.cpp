///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World 
///
/// This is an Object Oriented C++ Hello World program
///
/// @file hello1.cpp
/// @version 1.0
/// 
/// @author Shawn Tamashiro <shawnmt@hawaii.edu>
/// @brief  Lab 04c - Hello World - EE 205 - Spr 2021
/// @date   14 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

using namespace std;

int main(){

   cout << "Hello World!" << endl;

   return 0;
}

